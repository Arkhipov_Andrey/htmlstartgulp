const gulp = require('gulp');
const server = require('gulp-server-livereload');
const sass = require('gulp-sass');
const sourcemaps = require('gulp-sourcemaps');
const autoprefixer = require('gulp-autoprefixer');
const gulpif = require('gulp-if');
const uglify = require('gulp-uglify');
const babel = require('gulp-babel');
const minifycss = require('gulp-csso');
const useref = require('gulp-useref');
const imagemin = require('gulp-imagemin');
const clean = require('gulp-clean');

// Live reload web server
gulp.task('webserver', () => {
  gulp.src('src').pipe(server({
    livereload: true,
    port: 8080,
    open: true,
    log: 'debug'
  }));
});

// Style
gulp.task('style', () => {
  return gulp.src('src/sass/**/*.sass')
    .pipe(sourcemaps.init())
    .pipe(sass().on('error', sass.logError))
    .pipe(autoprefixer({browsers: ['last 2 versions']}))
    .pipe(sourcemaps.write())
    .pipe(gulp.dest('src/css'));
});

// css and js min
gulp.task('min', () => {
  gulp.src('src/*.html')
    .pipe(useref())
    .pipe(gulpif('*.js', babel({presets: ['env']})))
    .pipe(gulpif('*.js', uglify()))
    .pipe(gulpif('*.css', minifycss()))
    .pipe(gulp.dest('dist/'));
});

// Image min
gulp.task('image', () => {
  gulp.src('src/img/*')
    .pipe(imagemin())
    .pipe(gulp.dest('dist/img'));
});

// Build
gulp.task('build', ['clean', 'image', 'min']);

// Watch
gulp.task('watch', () => {
  gulp.watch('src/sass/**/*.sass', ['style'])
});

gulp.task('clean', () => {
  gulp.src('dist/', {read: false})
    .pipe(clean());
});

// Default
gulp.task('default', ['webserver', 'watch']);